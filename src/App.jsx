import { useState } from 'react'


import './App.css'
import { Footer } from './components/footer/Footer'
import { Nav } from './components/nav/Nav'
import { Main } from './components/main/Main'

function App() {
  const [count, setCount] = useState(0)

  if (count > 20)
    setCount(0)
  
  return (
    <>
      <Nav />
      <Main theContent = "Mesa Aula 3"/>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          Clique aqui para somar {count}
        </button>
      </div>
      <Footer />
    </>
  )
}

export default App
