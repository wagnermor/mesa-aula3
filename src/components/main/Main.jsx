import './Main.css'

// eslint-disable-next-line react/prop-types
export function Main({theContent}) {
  return (
    <div>
      <h1>
        {theContent}
      </h1>
    </div>
  )
}
