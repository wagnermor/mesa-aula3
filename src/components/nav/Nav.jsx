import { useState } from 'react';
import './Nav.css';

export function Nav() {
  const [menuSessao, setMenuSessao] = useState("Home")
  function handleClick(texto) {
    console.log(texto)
    // eslint-disable-next-line react/prop-types
    setMenuSessao(texto);
  }

  return (
    <div>
      <ul>
        <li onClick={() => handleClick("Home")}>Home</li>
        <li onClick={() => handleClick("Sobre nós clicado")}>Sobre</li>
        <li onClick={() => handleClick("Produtos clicado")}>Produtos</li>
        <li onClick={() => handleClick("Contato clicado")}>Contato</li>
      </ul>
      <div>
        <h2>{menuSessao}</h2>
      </div>
    </div>
  )
}
